package stepdefs;

import java.util.concurrent.TimeUnit;

import runner.SmartBoxCart;

import org.mortbay.log.Log;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.Cartpage;
import pageObjects.Homepage;
import pageObjects.Productlist;

public class StepDefinitions {

	Homepage homepage = new Homepage(SmartBoxCart.driver);
	Cartpage cartpage = new Cartpage(SmartBoxCart.driver);
	Productlist productlist = new Productlist(SmartBoxCart.driver);

	//Background: 
	@Given("^User on the SmartBox France page on URL www.smartbox.com/fr$")
	public void i_am_on_the_SmartBox_France_page_on_URL_smartbox() throws Throwable{
		SmartBoxCart.driver.get("https://www.smartbox.com/fr/");
	}

	@And("^User sees Cookies information and click on Accept cookies$")
	public void i_see_cookies_information_and_click_on_accept_cookies() throws Throwable{
		homepage.accept_Cookies();
	}

	@Then("^Cookies pages is closed$")
	public void i_see_cookies_page_is_closed(){
		homepage.cookies_NotDisplayed();
	}

	@Given("^User is on Home Page and hovers one the category$")
	public void user_is_on_home_page()  {
		homepage.homePage_Confirmation();
		homepage.select_Categories();
	}

	@When("^I select one of the subcategory$")
	public void i_select_one_subcategory() throws Throwable { 
		homepage.select_SubCategories();
	}

	@Then("^all the products are displayed$")
	public void select_first_option() throws Throwable {
		productlist.validate_Products_Displayed_OrNot();
	}

	@And("^select one of the product$")
	public void select_one_product() throws Throwable {
		productlist.select_Product();
	}

	@When("^I click on Ajouter au panier to add to cart$")
	public void i_click_on_add_to_cart() throws Throwable {
		productlist.clickOn_AddToCart(false);
	}

	@And("^options Continuer mes Achats and Voir le Panier are displayed")
	public void i_should_see_the_button() {
		try {
			productlist.check_Overlay_buttons();
		}catch(Exception e) {
			Log.info("Error in displaying buttons on Overlay: " +e);
		}
	}

	@And("^click on Voir le Panier to add product in shopping cart$")
	public void click_to_addto_cart() throws Throwable {
		productlist.clickOn_AddToCart(true);
	}

	@And("^select to Add five more products of same title$")
	public void select_Add_five_more_products() throws InterruptedException
	{
		productlist.add_More_Boxes();
	}

	@And("^Click on Passer La Commande$")
	public void click_to_buy_products() throws InterruptedException
	{
		cartpage.clickOn_CheckOut();
	}

	@Then("^It will show error that more than five products of same title are not allowed to purchase$")
	public void error_shown()
	{
	    cartpage.error_CheckOut();
	}

	@And("^Click on delete icon$")
	public void click_delete()
	{
		cartpage.delete_Product();
	}

	@Then("^Product will be removed from the shopping cart$")
	public void product_deleted()
	{
		homepage.delete_Product_Validation();
	}

	// Scenario : @FunctionalTestforBoxesdisplay
	@Given("^User is on Home Page$")
	public void user_on_homepage()  throws Throwable {
		homepage.homePage_Confirmation();
	}

	@When("^he/She search for product \"([^\\\"]*)\"$")
	public void i_select_one_subcategory(String search) throws Throwable { 
		homepage.perform_Search(search);
	}

	@Then("^Suggestions are displayed for customer$")
	public void suggestions_displayed_based_onsearch() throws Throwable {
		homepage.validate_Suggestions();
	}

	@And("^Hover on Gastronomie to find all sub categories$")
	public void to_find_subcategories() throws Throwable {
		homepage.select_Categories();
	}

	@And("^select one of the sub category$")
	public void select_subCategory() throws InterruptedException{
		homepage.select_SubCategories();
	}

	@Then("^products are displyed based on filter$")
	public void products_displayed() {
		try {
			productlist.validate_Products_Displayed_OrNot();
		}
		catch(Exception e)
		{
			Log.info("Error in reterieving products: "+e);
		}
	}

	@And("^search for a box based by name on filter \"([^\\\\\\\"]*)\"$")
	public void search_for_box(String filter){
		productlist.filter_Search(filter);
	}

	@Then("^Suggestions are displayed$")
	public void suggestions_displayed(){
		productlist.filter_Suggestions();
	}

	@And("^click on one of the box$")
	public void click_on_box(){
		productlist.select_Product();
	}

	@And("^click on reviews section$")
	public void click_on_review() {
		productlist.review_Page();
	}

	@Then("^it will take to reviews section$")
	public void reviews_section() throws Exception {
		productlist.review_Validation();
	}
}
