package helpers;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import runner.SmartBoxCart;

public class Helpers {

	public WebDriverWait wait = new WebDriverWait(SmartBoxCart.driver, 5);
	public Actions actions = new Actions(SmartBoxCart.driver);
	public JavascriptExecutor jse = (JavascriptExecutor)SmartBoxCart.driver;
}
