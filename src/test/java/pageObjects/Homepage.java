package pageObjects;

import java.util.concurrent.TimeUnit;

import org.mortbay.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import runner.SmartBoxCart;
import stepdefs.StepDefinitions;
import helpers.Helpers;

public class Homepage {

	Helpers helpers = new Helpers();
	
	//region PageElements
	@FindBy(how = How.XPATH, using = "/html/body/div[1]/div[2]/div[4]/div[2]/div") 
	private WebElement accept_Cookies;

	@FindBy(how = How.XPATH, using = "//*[@id=\"search\"]/div/input")
	private WebElement search_Product;

	@FindBy(how = How.XPATH, using = "//*[@id=\"search\"]/div/ul/li[1]/a")
	private WebElement suggestions_Shown1;

	@FindBy(how = How.XPATH, using = "//*[@id=\"search\"]/div/ul/li[2]/a")
	private WebElement suggestions_Shown2;

	@FindBy(how = How.XPATH, using = "//*[@id=\"gift-finder\"]/h2")
	private WebElement home_Page_Reference;

	@FindBy(how = How.XPATH, using = "//*[@id=\"header-dropdown-wrapper\"]/div[2]/div/span[1]/span")
	private WebElement cart_Validation;

	@FindBy(how = How.LINK_TEXT, using = "Gastronomie")
	private WebElement category;	

	@FindBy(how = How.XPATH, using = "//*[@id=\"qa-megamenu-food\"]/ul/li[1]/a/strong")
	private WebElement sub_Category1;
	//endregion

	public Homepage(WebDriver driver) {
		SmartBoxCart.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void perform_Search(String search) {
		
		helpers.wait.until(ExpectedConditions.elementToBeClickable(search_Product));
		search_Product.click();
		search_Product.sendKeys(search);
	}

	public void select_Categories() {

		helpers.actions.moveToElement(category).build().perform();
	}

	public void select_SubCategories() throws InterruptedException {
		helpers.wait.until(ExpectedConditions.visibilityOf(sub_Category1));
		sub_Category1.click();
	}

	public void accept_Cookies() {
		try {
			helpers.jse.executeScript("window.scrollTo(0,"+accept_Cookies.getLocation().y+")"); 
			if(accept_Cookies.isDisplayed())
			{
				accept_Cookies.click();
			}
			else
			{
				Log.info("Accept cookies is not displayed in homepage");
			}
		}
		catch(Exception e)
		{
			Log.info("Error in finding element: "+e);
		}		
	}

	public void cookies_NotDisplayed() {
		try {
			if(!accept_Cookies.isDisplayed())
			{
				Log.info("Cookies are not displayed");
			}
			else
			{
				Log.info("Accept cookies are still displayed in homepage");
			}
		}
		catch(Exception e)
		{
			Log.info("Error in finding element: "+e);
		}	
	}

	public void select_product_From_Options() {
		helpers.actions.moveToElement(suggestions_Shown1).perform();
		suggestions_Shown1.click();
	}

	public boolean validate_Suggestions() {

		if(suggestions_Shown1.isDisplayed() && suggestions_Shown2.isDisplayed())
		{
			if(suggestions_Shown1.getText().contains("Gastronomie") && suggestions_Shown2.getText().contains("Gastronomie"))
			{
				Log.info("Suggestions are validated");
			}
			search_Product.click();

			return true;
		}
		else
		{
			return false;
		}

	}

	public void homePage_Confirmation() {	

		if(home_Page_Reference.isDisplayed())
		{
			Log.info("User is in HomePage");
		}
	}

	public boolean delete_Product_Validation() {
		if(cart_Validation.getText() == "Vous n'avez pas de produit dans votre panier") {			
			return true;
		}

		else
		{
			Log.info("Cart empty message not shown");;
			return false;
		}
	}
}


