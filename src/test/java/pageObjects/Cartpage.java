package pageObjects;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.mortbay.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.common.base.Function;

import helpers.Helpers;
import runner.SmartBoxCart;

public class Cartpage {

	public Cartpage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	Helpers helpers = new Helpers();

	//region PageElements
	@FindBy(how = How.XPATH, using = "//*[@id=\"form-product-556504\"]/div/div[2]/div/div[1]/div/div/div[2]/a/i") 
	private WebElement cart_Delete_product;

	@FindBy(how = How.XPATH, using = "//*[@id=\"sticky\"]/div/div[1]/div[2]/div[6]/button/span") 
	private WebElement to_Order;

	@FindBy(how = How.XPATH, using = "//*[@id=\"form-product-160984\"]/div/div[2]/div/div[1]/div/div/div[2]/a/i") 
	private WebElement delete_Product_From_Cart;	

	@FindBy(how = How.XPATH, using = "//*[@id=\"cart-remove-confirm-accept\"]") 
	private WebElement confirm_Delete;

	@FindBy(how = How.XPATH, using = "//*[@id=\"form-product-160984\"]/div/div[1]/div/h5/a") 
	private WebElement product_InCart;

	@FindBy(how = How.XPATH, using = "//*[@id=\"quote-item-version-qty-160984\"]/option[6]") 
	private WebElement select_Toadd_Moreproducts;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"sticky\"]/div/div[1]/div[2]/div[6]/button/span")
	private WebElement add_To_Buy;
	
	@FindBy(how = How.ID, using = "checkout-email-address")
	private WebElement check_Email_Address;
	
	//endregion
	
	public void clickOn_CheckOut() {
		try {
		helpers.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@id=\\\"sticky\\\"]/div/div[1]/div[2]/div[6]/button/span")));
		add_To_Buy.click();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public void delete_Product(){
		try {
		helpers.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@id=\\\"form-product-160984\\\"]/div/div[2]/div/div[1]/div/div/div[2]/a/i")));
		delete_Product_From_Cart.click();
		helpers.actions.moveToElement(confirm_Delete).perform();
		confirm_Delete.click();		
	}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void error_CheckOut() {
		try {
			String assertion = "Error page will be displayed for order more than 5 boxes of same title";
			Assert.assertFalse(check_Email_Address.isDisplayed(), assertion);
	}		
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
