package pageObjects;

import java.util.List;

import org.mortbay.log.Log;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import helpers.Helpers;
import runner.SmartBoxCart;

public class Productlist {

	public Productlist(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}	

	Helpers helpers = new Helpers();
	
	//region PageElements
	@FindBy(how = How.XPATH, using = "//*[@id=\"box-type-ebox-classic\"]/div/a/span/span") 
	private WebElement product_AddToCart;

	@FindBy(how = How.XPATH, using = "//*[@id=\"addtocart-confirmation\"]/div/div/div[2]/a[2]") 
	private WebElement overlay_Confirmation;

	@FindBy(how = How.XPATH, using = "//*[@id=\"addtocart-confirmation\"]/div/div/div[2]/a[1]") 
	private WebElement overlay_Reject;

	@FindBy(how = How.XPATH, using = "//*[@id=\"ac-cloudSearchResults\"]/article[1]/a/div[4]/div[2]") 
	private WebElement select_product;

	@FindBy(how = How.XPATH, using = "//*[@id=\"box-product-info\"]/a[1]/span/i") 
	private WebElement reviews_select;

	@FindBy(how = How.XPATH, using = "//*[@id=\"sort-reviews\"]/option[1]")
	private WebElement reviewpage_validation;

	@FindBy(how = How.ID, using = "location")
	private WebElement filter_Click;

	@FindBy(how = How.XPATH, using = "//*[@id=\"ac-cloudSearchResults\"]/article[1]")
	private WebElement product_displayed1;

	@FindBy(how = How.XPATH, using = "//*[@id=\"cs-content\"]/section/header/div[3]/span[1]")
	private WebElement product_numbers_Displayed;

	@FindBy(how = How.CLASS_NAME, using = "quote-item-version-qty")
	private WebElement click_On_Items;

	@FindBy(how = How.XPATH, using = "//*[@id=\"quote-item-version-qty-160984\"]/option[6]")
	private WebElement select_More_Boxes;
	
	//endregion

	Actions actions = new Actions(SmartBoxCart.driver);

	public void clickOn_AddToCart(boolean overlayDisplayed) 
	{
		if(overlayDisplayed == false)
		{
			product_AddToCart.click();
		}
		else
		{
			overlay_Confirmation.click();
		}
	}

	public void check_Overlay_buttons() throws Exception
	{
		helpers.wait.until(ExpectedConditions.visibilityOf(overlay_Confirmation));
		if(overlay_Confirmation.isDisplayed() && overlay_Reject.isDisplayed())
		{
			Log.info("Both buttons are displayed");
		}
	}

	public void validate_Products_Displayed_OrNot() 
	{
		String number = product_numbers_Displayed.getText();
		if(number == "0" && product_displayed1.isDisplayed())
		{
			Log.info("Product(s) is/are displayed");
		}
	}

	public void filter_Search(String filter) {
		helpers.wait.until(ExpectedConditions.visibilityOf(filter_Click));
		filter_Click.click();
		filter_Click.sendKeys(filter);
		Log.info("Filter search is: "+ filter);
	}

	public void select_Product() {
		actions.moveToElement(select_product).perform();;
		helpers.wait.until(ExpectedConditions.visibilityOf(select_product));
		select_product.click();
	}

	public void add_More_Boxes() throws InterruptedException {

		click_On_Items.click();
		select_More_Boxes.click();
	}

	public void review_Page() {
		reviews_select.click();
	}

	public boolean review_Validation() {

		if(reviewpage_validation.isDisplayed()){
			Log.info("Review page is displayed");
			return true;
		}
		else
		{
			Log.info("Review page is not displayed");
			return false;
		}
	}

	public void filter_Suggestions() {

		try {
			// BUG : To check for suggestions displayed when user types box in filter. Couldn't find any element for filter suggestions
		}
		catch(Exception e) {
			Log.info("Filter suggestions are not displayed when user is looking for a box \"Week-end gourmand en amoureux\" and user need to manually click on search icon: "+e);
		}
	}
}
