@FunctionalTestforBoxCart
Feature: As a Customer 
I want to fine a desired box and add it to cart
So i can make adjustments prior to checkout

 Background: User navigates to Company home page
 Given User on the SmartBox France page on URL www.smartbox.com/fr
 And User sees Cookies information and click on Accept cookies
 Then Cookies pages is closed
    
  Scenario: Add Box to Cart 
    Given User is on Home Page and hovers one the category
    When I select one of the subcategory 
    Then all the products are displayed
    And select one of the product
    And I click on Ajouter au panier to add to cart
    Then options Continuer mes Achats and Voir le Panier are displayed
    And click on Voir le Panier to add product in shopping cart
    And select to Add five more products of same title
    And Click on Passer La Commande
    Then It will show error that more than five products of same title are not allowed to purchase
    And Click on delete icon
    Then Product will be removed from the shopping cart
