@FunctionalTestforBoxesdisplay
Feature: As a Customer 
I want to view a list of products
So i can select some to purchase

Background: User navigates to Company home page
 Given User on the SmartBox France page on URL www.smartbox.com/fr
 And User sees Cookies information and click on Accept cookies
 Then Cookies pages is closed
 
  Scenario: Boxes display based on search item 
    Given User is on Home Page
    When he/She search for product "Gastronomie" 
    Then Suggestions are displayed for customer
    And Hover on Gastronomie to find all sub categories
    And select one of the sub category
    Then products are displyed based on filter
    And search for a box based by name on filter "Week-end gourmand en amoureux"
    Then Suggestions are displayed
    And click on one of the box
    And click on reviews section
    Then it will take to reviews section